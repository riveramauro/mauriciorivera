<?php
// Template Name: Portfolio
?>
<?php get_header(); ?>


<?php $port_query = new WP_Query ('post_type=portfolio'); ?>

<ul id="post-<?php the_ID(); ?>" class="portfolio-grid small-block-grid-1 large-block-grid-3">
<?php if ($port_query->have_posts() ) : ?>
	<?php while ($port_query->have_posts() ) : $port_query->the_post(); ?>
		<li>
			<div class="portfolio-thumb">
		        <div class="outter">
		            <img src="<?php the_field('portfolio_thumbnail'); ?>" alt="">
		        </div>
		        <a href="<?php the_permalink(); ?>" class="inner">
		            <div class="center-container is-table">
		                <div class="table-cell">
		                    <div class="center-block">
		                        <h3><?php the_title( $before = '', $after = '', $echo = true ); ?></h3>
		                        <span><?php the_field('creative_fields'); ?></span>
		                        <hr>
		                    </div>
		                </div>
		            </div>
		        </a>
		    </div>
		</li>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
<?php endif ?>
</ul>
<?php get_footer(); ?>