<?php
// Template Name: About
?>
<?php get_header(); ?>

<div class="about fill">
	<div class="about-container">
		<h2 class="title">I’m a <span class="gold">designer</span> of all sorts</h2>
		<p>My name is Mauricio Rivera, a creator of things since I can remember and native from the hills of Bogota, Colombia. I became captivated by good design when I first discovered that street art was more than scribbles on a wall. My obsession with “Obey”, Bansky and Blek le Rat made me discover a whole new world. That journey took me to graphic design, and that became my way of life.</p>
		<p>I love the craft of designing and finding visual solutions, developing new ideas and thinking of ways to change the world. Might sound a bit ambitious but that's my nature.</p>
		<div class="quote">
			<p>I like to live by the motto:</p>
			<div class="moto">
				“...perfection is finally attained <br>
				not when there is no longer anything to add, <br>
				but when there is no longer anything to take away...” <br>
				<span>- Antoine de Saint-Exupéry</span>
			</div>
		</div>
	</div>
</div>



<?php get_footer(); ?>