<?php
/**
 * @package MR Portfolio
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('blog-single'); ?>>
	<header class="entry-header">
		<div class="title">
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content row">
		<div class="small-12 columns">
			<?php the_content(); ?>
		</div>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'mrportfolio' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<div class="row">
		<div class="small-12 columns"><?php mrportfolio_content_nav( 'nav-below' ); ?></div>
	</div>

</article><!-- #post-## -->
