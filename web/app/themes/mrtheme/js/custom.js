jQuery(function($){
	// class="Fill" to fill the window with div
	// var $winHeight = $(window).height() - 50;
	// $('.fill').css('height', $winHeight);

	$('.hide').css('opacity', '0');
	// whole page animation 
	$(document).ready(function(){
		var theBody = $('#page');
		// $(theBody).css('opacity', '0');
		$('.load').transition({opacity: 0}, 2000, 'easeInOutBack', function(){
			$(this).css('display', 'none');
		});
		$("a").click(function(event){
		event.preventDefault();
		linkLocation = this.href;
		$(theBody).transition({opacity: 0 }, 1000, redirectPage);
		});
			
		function redirectPage() {
			window.location = linkLocation;
		}
		// $(theBody).transition({ opacity: 1 }, 2000, 'easeInOutBack');
	});
	$('.logo').transition({rotate: '360deg', delay: 1000, duration: 3000}, function(){
		var $titles = $('h1.site-title, h3.site-description, a.mr-button');
		$titles.transition({opacity: 1});
	});

	// Portfolio thumbnails
	$('.portfolio-thumb').hover(function() {
		$('.inner',this).transition({opacity: 0.9});
		$('hr',this).transition({width: '80%'});
	}, function() {
		$('.inner',this).transition({opacity: 0});
		$('hr',this).transition({width: '0'});
	});
});