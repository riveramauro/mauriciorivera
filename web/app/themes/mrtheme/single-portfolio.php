<?php
/**
 * The Template for displaying portfolio posts.
 *
 * @package MR Portfolio
 */

get_header(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="title">
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<div class="project-type"><?php the_field('creative_fields'); ?></div>
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content row">
		<div class="small-12 columns">
			<?php echo apply_filters('the_content', $post->post_content); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'mrportfolio' ),
				'after'  => '</div>',
			) );
		?>
		</div>
		<div class="small-12 columns">
		<?php /* check for images and display */?>
		<?php foreach (range(1,10) as $digit) :
			$image = get_field('image_'. $digit);
			if ($image) :
				$url = $image['url'];
				$alt = $image['alt'];	?>
			<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
		<?php endif; endforeach; ?>
			
		</div>
		<div class="small-12 columns credits">
			<div><strong class="title">Client:</strong> <?php the_field('client'); ?></div>
			<div><strong class="title">Agency:</strong> <?php the_field('studio'); ?></div>
		</div>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'mrportfolio' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->

<?php get_footer(); ?>