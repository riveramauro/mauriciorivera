<?php
/**
 * @package MR Portfolio
 */
// Blog page
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('blog-list'); ?>>
	<header class="entry-header cf">
		<h1 class="entry-title title left"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta date">
			| <?php the_time('m . d . Y') ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
		<span class="tags-links right">
		<?php
				/* translators: used between list items, there is a space after the comma */
				// $tags_list = get_the_tag_list( '', __( ', ', 'mrportfolio' ) );
				// if ( $tags_list ) :

				$tags_list = get_the_tags();
				$theme_url = home_url();
				if ($tags_list) {
					foreach($tags_list as $tag){
						echo '<a class="tags" href="' . $theme_url. '/tag/' . $tag->name .'">#'. $tag->name . '</a>';
					}
				}
			?>
		</span>
			
	</header><!-- .entry-header -->
</article><!-- #post-## -->