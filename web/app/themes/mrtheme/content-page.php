<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package MR Portfolio
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
	<div class="small-12 columns" style="margin-top: 4%;">
		<header class="entry-header">
			<h1 class="entry-title text-center"><?php the_title(); ?></h1>
		</header><!-- .entry-header -->
		
		<div class="entry-content">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'mrportfolio' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
		<?php edit_post_link( __( 'Edit', 'mrportfolio' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
		</div>
</article><!-- #post-## -->
