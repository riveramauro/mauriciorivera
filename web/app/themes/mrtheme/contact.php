<?php
// Template Name: Contact
?>
<?php get_header(); ?>

<div class="contact fill">
	<div class="contact-container">
		<h2 class="title">Catch me if you can</h2>
		<p>Have a project in mind? Or just want to say hello!? <br>You can find me on any of the places below</p>
		<div class="social">
			<a href="mailto:me@mauriciorivera.co"><div class="icon mail"></div></a>
			<a href="http://twitter.com/riveramauro"><div class="icon twitter"></div></a>
			<a href="http://instagram.com/speakoutyourmind"><div class="icon instagram"></div></a>
			<a href="http://www.behance.net/mauriciorivera"><div class="icon behance"></div></a>
			<a href="http://www.linkedin.com/in/riveramauro"><div class="icon linkedin"></div></a>
		</div>
	</div>
</div>



<?php get_footer(); ?>