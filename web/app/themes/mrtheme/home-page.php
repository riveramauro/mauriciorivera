<?php
// Template Name: Home
?>
<?php get_header(); ?>

<div class="site-branding">
	<div class="brand-container">
		<img class="logo" src="<?php bloginfo('template_url'); ?>/img/mr-logo.png" alt="Mauricio Rivera">
		<h1 class="site-title title"><?php bloginfo( 'name' ); ?></h1>
		<h3 class="site-description"><?php bloginfo( 'description' ); ?></h3>
		<a class="mr-button" href="<?php bloginfo('url'); ?>/portfolio">View selected projects</a>
	</div>
</div>


<?php get_footer(); ?>