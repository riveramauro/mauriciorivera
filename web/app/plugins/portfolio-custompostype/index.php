<?php
/*
Plugin Name: Portfolio Custom Post Types
Description: Custom Post Types for this website.
Author: Mauricio Rivera
Author URI: http://www.mauriciorivera.co
*/

add_action( 'init', 'portfolio_cpt' );

function portfolio_cpt() {

register_post_type( 'portfolio', array(
  'labels' => array(
    'name' => 'Portfolio',
    'singular_name' => 'Portfolio',
   ),
  'description' => 'Portfolio pieces',
  'public' => true,
  'menu_position' => 20,
  'has_archive' => false,
  'show_in_menu' => true,
  'supports' => array( 'title', 'editor', 'custom-fields' )
));
}

?>